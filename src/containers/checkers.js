import React, { Component } from 'react';
import { connect } from 'react-redux';

import FormCheckers from '../components/formCheckers';
import Board from '../components/board';
import { buildBoard } from '../actions';

export class Checkers extends Component {
  componentDidMount() {
    const { rowsAmount, colsAmount } = this.props;

    this.props.buildBoard(rowsAmount, colsAmount);
  }

  render() {
    return (
      <div className='checkers-game'>
        <div className='game-header'>
          <img src='/img/appsflyer.png' alt='appsflyer icon' />
          <span className='game-title'>AppsFlyer Checkers</span>
        </div>
        <FormCheckers />
        <Board />
      </div>
    );
  }
}

Checkers.defaultProps = {
  rowsAmount: 8,
  colsAmount: 8
};

const mapDispatchToProps = dispatch => {
  return {
    buildBoard: (rowsAmount, colsAmount) => {
      dispatch(buildBoard(rowsAmount, colsAmount));
    }
  };
};

export default connect(null, mapDispatchToProps)(Checkers);
