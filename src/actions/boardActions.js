import {
  BUILD_BOARD, TOGGLE_SOLDIER, ADD_SOLDIER, REMOVE_SOLDIER
} from './types';

export const buildBoard = (rowsAmount, colsAmount) => {
  return {
    type: BUILD_BOARD,
    payload: {
      rowsAmount,
      colsAmount
    }
  }
}

export const addSoldier = (rowNumber, colNumber) => {
  return {
    type: ADD_SOLDIER,
    payload: {
      rowNumber,
      colNumber
    }
  }
}

export const removeSoldier = (rowNumber, colNumber) => {
  return {
    type: REMOVE_SOLDIER,
    payload: {
      rowNumber,
      colNumber
    }
  }
}

export const toggleSoldier = (rowNumber, colNumber) => {
  return {
    type: TOGGLE_SOLDIER,
    payload: {
      rowNumber,
      colNumber
    }
  }
}