import {
  buildBoard, toggleSoldier, addSoldier, removeSoldier
} from './boardActions';

export {
  buildBoard, toggleSoldier, addSoldier, removeSoldier
};