import React, { Component } from 'react';
import { connect } from 'react-redux';

import Cell from './cell';

export class Row extends Component {
  render() {
    const { board, rowNumber } = this.props;
    const isBoard = board.length > 0;

    return (
      <div className='row'>
        {isBoard && board[rowNumber - 1].map((cell, index) => {
          return <Cell rowNumber={rowNumber} cellNumber={index + 1} key={index} />
        })}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { checkers = {} } = state;
  const { board = [] } = checkers;

  return {
    board
  }
};

export default connect(mapStateToProps)(Row);