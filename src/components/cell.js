import React, { Component } from 'react'
import { connect } from 'react-redux';

import Soldier from './soldier';
import { toggleSoldier } from '../actions';

export class Cell extends Component {
  onToggleClick = () => {
    const { rowNumber, cellNumber } = this.props;

    this.props.toggleSoldier(rowNumber, cellNumber)
  }
  render() {
    const { board, rowNumber, cellNumber } = this.props;
    const isBoardCreated = board.length > 0;

    return (
      <div className='cell' onClick={this.onToggleClick}>
        {isBoardCreated && board[rowNumber - 1][cellNumber - 1] && <Soldier />}
      </div>

    )
  }
}

const mapStateToProps = (state) => {
  const { checkers = {} } = state;
  const { board = [] } = checkers;

  return {
    board
  }
};

const mapDispatchToProps = dispatch => {
  return {
    toggleSoldier: (rowNumber, colNumber) => {
      dispatch(toggleSoldier(rowNumber, colNumber))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cell);