import React, { Component } from 'react'
import { connect } from 'react-redux';
import { addSoldier, removeSoldier } from '../actions';

export class FormCheckers extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAddEnable: true,
      isRemoveEnable: false
    };

    this.txtRow = React.createRef();
    this.txtCol = React.createRef();
    this.btnAdd = React.createRef();
    this.btnRemove = React.createRef();
  }

  onAddClick = (ev) => {
    this.props.addSoldier(this.txtRow.current.value, this.txtCol.current.value);
    this.checkButtonsEnable(this.props);
    ev.stopPropagation();
  }

  onRemoveClick = (ev) => {
    this.props.removeSoldier(this.txtRow.current.value, this.txtCol.current.value);
    this.checkButtonsEnable(this.props);
    ev.stopPropagation();
  }
  componentWillReceiveProps(nextProps) {
    const { board } = nextProps;
    const isBoard = board.length > 0;

    isBoard && this.checkButtonsEnable(nextProps);
  }

  onInputChange = (ev) => {
    this.checkButtonsEnable(this.props);
  }

  checkButtonsEnable = (props) => {
    const { board } = props;
    const rowIndex = parseInt(this.txtRow.current.value) - 1;
    const cellIndex = parseInt(this.txtCol.current.value) - 1;

    this.setState({
      isAddEnable: !board[rowIndex][cellIndex],
      isRemoveEnable: board[rowIndex][cellIndex]
    })
  }

  render() {
    return (
      <div className='game-form'>
        <div className='fields'>
          <div className='field'>
            <label>row number:</label>
            <input type='number' ref={this.txtRow}
              defaultValue='1' min='1' max='8' onChange={this.onInputChange} onKeyDown={(ev) => { ev.preventDefault(); }} />
          </div>
          <div className='field'>
            <label>cell number:</label>
            <input type='number' ref={this.txtCol}
              defaultValue='1' min='1' max='8' onChange={this.onInputChange} onKeyDown={(ev) => { ev.preventDefault(); }} />
          </div>
        </div>
        <div className='buttons'>
          <button ref={this.btnAdd} disabled={!this.state.isAddEnable} onClick={this.onAddClick}>Add</button>
          <button ref={this.btnRemove} disabled={!this.state.isRemoveEnable} onClick={this.onRemoveClick}>Remove</button>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addSoldier: (rowNumber, colNumber) => {
      dispatch(addSoldier(parseInt(rowNumber), parseInt(colNumber)))
    },
    removeSoldier: (rowNumber, colNumber) => {
      dispatch(removeSoldier(parseInt(rowNumber), parseInt(colNumber)))
    }
  };
};

const mapStateToProps = (state) => {
  const { checkers = {} } = state;
  const { board } = checkers;

  return {
    board
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FormCheckers);